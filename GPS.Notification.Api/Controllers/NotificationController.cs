﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace GPS.Core
{
    [ApiController]
    [Route("[controller]")]
    public class NotificationController : ControllerBase
    {
        private readonly ILogger<NotificationController> _logger;

        public NotificationController(ILogger<NotificationController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> SendNotification([FromBody] NotificationRequest request)
        {
            var smsRequest = new SmsRequest(request);

            var client = new HttpClient();
            var requestMessage = new HttpRequestMessage(HttpMethod.Post, "https://localhost:6001/sms");
            requestMessage.Content = new StringContent(JsonConvert.SerializeObject(smsRequest), Encoding.UTF8, "application/json");

            var response = await client.SendAsync(requestMessage);
            var notificationResponse = NotificationResponse.ConvertToResponse(request);
            notificationResponse.Message = smsRequest.Message;
            notificationResponse.IsSuccess = response.IsSuccessStatusCode;

            return Ok(notificationResponse);
        }

        [HttpGet("status/{id}")]
        public async Task<IActionResult> Get([FromRoute] string id)
        {
            var client = new HttpClient();
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, $"https://localhost:6001/sms/status/{id}");

            var response = await client.SendAsync(requestMessage);

            var responseStatusItem = JsonConvert.DeserializeObject<StatusItem>(await response.Content.ReadAsStringAsync());

            if (response.IsSuccessStatusCode)
            {
                return Ok(new NotificationStatusItem
                {
                    IsProcesssed = responseStatusItem.IsProcesssed,
                    NotificationRequestId = responseStatusItem.NotificationRequestId,
                    Sent = responseStatusItem.Sent
                });
            }

            return NoContent();
        }
    }
}
