﻿using System;

namespace GPS.Core
{
    public interface IStorage
    {
        void Add(StatusItem statusItem);
        StatusItem Get(Guid notificationRequestId);
    }
}
