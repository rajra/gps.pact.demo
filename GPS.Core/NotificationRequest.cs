﻿using System;

namespace GPS.Core
{
    public class NotificationRequest
    {
        public SmsType MessageType { get; set; }
        public string ToMobile { get; set; }
        public Guid Id { get; set; }
    }
}