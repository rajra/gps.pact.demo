﻿using System;

namespace GPS.Core
{
    public class SmsRequest
    {
        public SmsRequest()
        {
        }

        public SmsRequest(NotificationRequest request)
        {
            Created = DateTime.Now;
            FromMobile = "123456789";
            Id = request.Id;

            if (request.MessageType == SmsType.Modulr)
            {
                Message = "Modulr notification message";
                ToMobile = "2233445566";
            }
            else
            {
                Message = "RDX notification message";
                ToMobile = "8855446622";
            }
        }

        public string ToMobile { get; set; }
        public string FromMobile { get; set; }
        public string Message { get; set; }
        public DateTime Created { get; set; }
        public Guid Id { get; set; }
    }
}