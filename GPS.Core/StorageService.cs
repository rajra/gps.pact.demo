﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GPS.Core
{
    public class StorageService : IStorage
    {
        private readonly List<StatusItem> _storage = new List<StatusItem>();

        public void Add(StatusItem statusItem)
        {
            if (!_storage.Exists(x => x.NotificationRequestId == statusItem.NotificationRequestId))
            {
                _storage.Add(statusItem);
            }
        }

        public StatusItem Get(Guid notificationRequestId)
        {
            var statusItem = _storage.FirstOrDefault(x => x.NotificationRequestId == notificationRequestId);

            return statusItem;
        }
    }
}