﻿using System;

namespace GPS.Core
{ 
    public class SmsResponse
    {
        public Guid SmsId { get; set; }
        public Guid RequestId { get; set; }
        public DateTime Created { get; set; }

        public static SmsResponse ConvertToResponse(SmsRequest request)
        {
            return new SmsResponse
            {
                SmsId = Guid.NewGuid(),
                RequestId = request.Id,
                Created = request.Created,
            };
        }
    }
}