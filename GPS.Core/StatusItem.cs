﻿using System;

namespace GPS.Core
{
    public class StatusItem
    {
        public Guid Id { get; set; }
        public Guid NotificationRequestId { get; set; }
        public bool IsProcesssed { get; set; }

        public DateTime Sent { get; set; }
    }

    public class NotificationStatusItem
    {
        public Guid NotificationRequestId { get; set; }
        public bool IsProcesssed { get; set; }

        public DateTime Sent { get; set; }
    }
}