﻿using System;

namespace GPS.Core
{
    public class NotificationResponse
    {
        public Guid RequestId { get; set; }
        public DateTime Created { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }

        public static NotificationResponse ConvertToResponse(NotificationRequest request)
        {
            return new NotificationResponse
            {
                RequestId = request.Id,
                Created = DateTime.Now,
                IsSuccess = false
            };
        }
    }
}