﻿using GPS.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace GPS.Sms.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SmsController : ControllerBase
    {
        private readonly ILogger<SmsController> _logger;
        private readonly IStorage storage;

        public SmsController(ILogger<SmsController> logger, IStorage storage)
        {
            _logger = logger;
            this.storage = storage;
        }

        [HttpPost]
        public IActionResult SendSms([FromBody] SmsRequest request)
        {
            var sms = SmsResponse.ConvertToResponse(request);

            storage.Add(new StatusItem
            {
                Id = sms.SmsId,
                NotificationRequestId = sms.RequestId,
                Sent = DateTime.Now,
            });

            return Ok(sms);
        }

        [HttpGet("status/{id}")]
        public IActionResult Get([FromRoute] string id)
        {
            var status = storage.Get(Guid.Parse(id));

            if (status == null)
            {
                return NoContent();
            }

            return Ok(status);
        }
    }
}
